let trainer = {
	name: "Ash Ketchum",
	age: 10,
	pokemon: ["Pikachu", "Pichu", "Raichu", "Mew"],
	friends: {
		hoenn: ["May", "Max"],
		kanto: ["Brock", "Misty"]
	},
	talk: function(){
		console.log(this.pokemon[0] + "! I choose you!")
	}
};

console.log(trainer);
console.log("Result of dot notation:");
console.log(trainer.name);
console.log("Result of square bracket notation:");
console.log(trainer.pokemon);
console.log(trainer.pokemon[3]);
console.log("Result of talk method")
trainer.talk();

function Pokemon(name, level){
	this.name = name,
	this.level = level,
	this.health = 2 * level,
	this.attack = level,
	this.tackle = function(target) {
		let damagedHealth = target.health - this.attack;
		console.log(this.name + " tackled " + target.name)
		console.log(target.name + "'s health is now reduced to" + " " + damagedHealth)

		if (damagedHealth <= 0) {
			target.faint();
		} else {
			target.health = damagedHealth;
		}
	},
	this.faint = function() {
		console.log(this.name + " fainted.")
	}
};

let pikachu = new Pokemon ("Pikachu", 12);
let geodude = new Pokemon ("Geodude", 8);
let mewtwo = new Pokemon ("Mewtwo", 100);

console.log(pikachu);
console.log(geodude);
console.log(mewtwo);
geodude.tackle(pikachu);
console.log(pikachu);
mewtwo.tackle(geodude);